import express from "express";
import result from "./slot.js";

const app = express();
const port = 3000;
app.use(express.json())

app.use(`/`, (req, res) => {
    res.json(result);
    console.log(result);
})


app.listen(port, () => {
    console.log(`server is listening`)
})
