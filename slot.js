import config from "./configuration.js";

const { reelsCount, rowsCount, symbols, lines, reels } = config;
const ZERO = 0;
const ONE = 1;
const TWO = 2;
const THREE = 3;
const FOUR = 4;
const FIVE = 5;
const DO_NOTHING = null;

class Slot {
    #result = {
        reels: {},
        payoutLines: {}
    };

    #calculateRandomIndex(length) {
        return Math.floor(Math.random() * length);
    }
    #calculatePayoutLine(line, [val1, val2, val3, val4, val5]) {
        const reel = [val1, val2, val3, val4, val5];

        line === ONE ? this.#result.payoutLines[line] = ZERO : DO_NOTHING;
        line === TWO ? this.#result.payoutLines[line] = reel.reduce((acc, val) => acc += val, ZERO) : DO_NOTHING;
        line === THREE ? this.#result.payoutLines[line] = reel.reduce((acc, val) => acc += val, ZERO) * TWO : DO_NOTHING;
        line === FOUR ? this.#result.payoutLines[line] = val2 + val4 : DO_NOTHING;
        line === FIVE ? this.#result.payoutLines[line] = (val1 + val3 + val5) + ((val2 + val4) * 2) : DO_NOTHING;
    }

    spin() {
        reels.map((reel, reelPosition) => {
            const randomSymbolIndex = this.#calculateRandomIndex(reel.length);
            const reelSymbols = symbols[reel[randomSymbolIndex]];
            const reelNumber = reelPosition + ONE;
            this.#result.reels[reelNumber] = reelSymbols;
            const currentReel = this.#result.reels[reelNumber];
            this.#calculatePayoutLine(reelNumber, currentReel);
        })
    }

    get result() {
        return this.#result;
    }
}

const machine = new Slot();
machine.spin()
export default machine.result;
